let start_at = document.getElementById("start_at");
let end_at = document.getElementById("end_at");
let show_minute = document.getElementById("show_minute");
let total_amount = document.getElementById("total_amount");
let startBtn = document.getElementById("startBtn");
let stopBtn = document.getElementById("stopBtn");
let clearBtn = document.getElementById("clearBtn");
var startDate;
var endDate;

stopBtn.style.display = "none";
clearBtn.style.display = "none";

startBtn.addEventListener("click", function () {

    startDate = new Date();
    start_at.innerHTML = new Date().toLocaleTimeString([], { timeStyle: 'short' });

    startBtn.style.display = "none";
    stopBtn.style.display = "block";
});

stopBtn.addEventListener("click", function () {

    endDate = new Date();
    end_at.innerHTML = new Date().toLocaleTimeString([], { timeStyle: 'short' });

    var minutes = Math.round(((endDate.getTime() - startDate.getTime()) / 1000) / 60);
    // var seconds = Math.round((endDate.getTime() - startDate.getTime()) / 1000);

    let counter = Math.floor(minutes / 60);
    let getRemain = Math.abs(minutes % 60);

    if (getRemain < 16) {
        document.getElementById("total_amount").innerHTML = (counter * 1500) + 500;
    }
    else if (getRemain < 31) {
        document.getElementById("total_amount").innerHTML = (counter * 1500) + 1000;
    }
    else if (getRemain < 60){
        document.getElementById("total_amount").innerHTML = (counter * 1500) + 1500;
    }

    document.getElementById("show_minute").innerHTML = minutes;

    stopBtn.style.display = "none";
    clearBtn.style.display = "block";
});

clearBtn.addEventListener("click", function () {
    start_at.innerHTML = "00:00";
    end_at.innerHTML = "00:00";
    show_minute.innerHTML = "0";
    total_amount.innerHTML = "0";
    clearBtn.style.display = "none";
    startBtn.style.display = "block";
});

setInterval(function () {
    document.getElementById("currentDate").innerHTML = new Date().toDateString() + " " + new Date().toLocaleTimeString();
}, 1000);